
memory.limit(size=35000)

#### ------------------------ packages ----------------------------##

#install.packages("data.table")
library(data.table)
#install.packages("dplyr")
library(dplyr)
#install.packages("magrittr")
library(magrittr)
#install.packages("anytime")
library(anytime)
#install.packages("SDMTools")
library(SDMTools)


####---------------------loading files------------------------------###

require(data.table) # v1.9.0+

event_data <- fread("events.csv")

documents_categories_data <- fread("documents_categories.csv")
documents_categories_data<-as.data.frame(documents_categories_data)

documents_topics_data <- fread("documents_topics.csv")
documents_topics_data<-as.data.frame(documents_topics_data)

promoted_content_data <-fread('promoted_content.csv')
clicks_train_data <-fread('clicks_train.csv')

clicks_train_data<-as.data.frame(clicks_train_data)

### ------------checking NA------------------

summary(clicks_train_data)
summary(promoted_content_data)
summary(documents_categories_data)
summary(documents_topics_data)


pMiss<-function(x){
  sum(is.na(x))/length(x)*100
}
sMiss<-function(x){
  sum(is.na(x))
}
apply(event_data,2,pMiss)
apply(documents_categories_data,2,sMiss)
apply(promoted_content_data,2,sMiss)
apply(documents_meta_data,2,sMiss)
apply(documents_topics_data,2,sMiss)
apply(clicks_train_data,2,sMiss)

## --------------------------fixing data-------------------------

#convert data on geo_location to real countries
event_data$geo_location_country<-event_data$geo_location
event_data$geo_location <- substr(event_data$geo_location, 1, 2)
event_data$geo_location_country <- substr(event_data$geo_location_country, 4, 5)

#convert data on timestamp to real time
event_data$timestamp <-(event_data$timestamp + 1465876799998) / 1000
event_data$timestamp<-anytime(event_data$timestamp)
#anydate(tmps)

event_clear_data<-event_data[,-2]

colnames(promoted_content_data)[2] <- "document_id_target"

####split the display ID for test and train
vec<-runif(nrow(event_clear_data)) 
split<-vec >0.3 
split

event_train<-event_clear_data[split,1] #the comma means we split by rows and not with columns.
event_test<-event_clear_data[!split,1] #! means convert the logical value. true become false and the opposit.
#event_sample<-event_test[1:6000,1]
#event_sample<-event_test[6000:12000,1]
#event_sample<-event_test[12000:18000,1]
event_sample<-event_test[18000:24000,1]

##### ----------------transport to data table for merge---------------------

setDT(promoted_content_data)
setDT(documents_categories_data)
setDT(documents_topics_data)
setDT(event_clear_data)
setDT(clicks_train_data)

## --------------------------merge for functions -------------------------

matching_data <-merge(clicks_train_data,event_clear_data, by='display_id')
colnames(matching_data)[4] <- "document_id_source"
matching_data <-merge(matching_data,promoted_content_data, by='ad_id')
summary(matching_data)
fwrite(matching_data, "matching_data.csv")

function_data <-merge(matching_data,event_sample, by='display_id')
function_distinct_data <-distinct(function_data,document_id_source,document_id_target,keep_all = FALSE)


###---------------------------------function satrt--------------####
#  categories functions

function_distinct_data$category_match <- numeric()
function_distinct_data$category_not_match <- numeric()
function_distinct_data$SUM_match_category <- numeric()
function_distinct_data$SUM_NOT_match_category <- numeric()
for(i in 1:nrow(function_distinct_data)){
  
  temp_cat_src <- filter(documents_categories_data, document_id == function_distinct_data$document_id_source[i])
  temp_cat_TAR <- filter(documents_categories_data, document_id == function_distinct_data$document_id_target[i])
  setDT(temp_cat_src)
  setDT(temp_cat_TAR)
  counter_src <- nrow(temp_cat_src) 
  counter_tar <- nrow(temp_cat_TAR) 
  matching_cat_num <-merge(temp_cat_src,temp_cat_TAR, by='category_id')
  setDT(matching_cat_num)
  function_distinct_data$category_match[i]<-nrow(matching_cat_num)
  
  counter_match <- nrow(matching_cat_num)
  
  function_distinct_data$category_not_match[i]<-(counter_src + counter_tar - (counter_match*2))
  
  function_distinct_data$SUM_match_category[i]<-(sum(matching_cat_num$confidence_level.x)+sum(matching_cat_num$confidence_level.y))
  
  cat_src_sum<-sum(temp_cat_src$confidence_level)
  cat_tar_sum<-sum(temp_cat_TAR$confidence_level)
  
  matching_sum <-sum(matching_cat_num$confidence_level.x)+sum(matching_cat_num$confidence_level.y)
  
  function_distinct_data$SUM_NOT_match_category[i]<-(cat_src_sum + cat_tar_sum - matching_sum)
  
  
}


# - topics functions
function_distinct_data$topics_match <- numeric()
function_distinct_data$topics_not_match <- numeric()
function_distinct_data$SUM_match_topics <- numeric()
function_distinct_data$SUM_NOT_match_topics <- numeric()
for(i in 1:nrow(function_distinct_data)){
  
  temp_top_src <- filter(documents_topics_data, document_id == function_distinct_data$document_id_source[i])
  temp_top_TAR <- filter(documents_topics_data, document_id == function_distinct_data$document_id_target[i])
  matching_top_num <-merge(temp_top_src,temp_top_TAR, by='topic_id')
  
  function_distinct_data$topics_match[i]<-nrow(matching_top_num)
  
  counter_src <- nrow(temp_top_src)
  counter_tar <- nrow(temp_top_TAR)
  counter_match <- nrow(matching_top_num)
  
  function_distinct_data$topics_not_match[i]<-(counter_src + counter_tar - (counter_match*2))
  
  function_distinct_data$SUM_match_topics[i]<-(sum(matching_top_num$confidence_level.x)+sum(matching_top_num$confidence_level.y))
  top_src_sum<-sum(temp_top_src$confidence_level)
  top_tar_sum<-sum(temp_top_TAR$confidence_level)
  
  matching_sum <-sum(matching_top_num$confidence_level.x)+sum(matching_top_num$confidence_level.y)
  
  function_distinct_data$SUM_NOT_match_topics[i]<-(top_src_sum + top_tar_sum - matching_sum)
  
}



# - number of ads in each display_id
display_id_counter <- data.frame(table(function_data$display_id))
colnames(display_id_counter) <- c("display_id", "Num_of_ads")
setDT(display_id_counter)

flat_file <-merge(function_data,function_distinct_data, by=c("document_id_source", "document_id_target"))
head(flat_file)
display_id_counter$display_id <- as.numeric(as.character(display_id_counter$display_id))


#write.table(function_distinct_data, file = 'function_distinct_data.csv', sep=',', col.names = TRUE, row.names = FALSE )


####________________________________END OF FUNCTION_______________________________#####




## -------------------------- merge num 3 = final flat file -------------------------

flat_file <-merge(flat_file,display_id_counter, by="display_id")

documents_meta_data <-fread('documents_meta.csv')
setDT(documents_meta_data)
colnames(documents_meta_data)[1]<-"document_id_source"
 
flat_file <-merge(flat_file,documents_meta_data, by='document_id_source')

#write.table(flat_file, file = 'flat_file_newwww.csv', sep=',', col.names = TRUE, row.names = FALSE )


###---prepering data for algo------------------------------

summary(flat_file)

time <-hour(flat_file$timestamp)


distinct_time<- function(x){
  if ((1<x)&(x<5)){
    x<- 1
  }
  else if ((5<x)&(x<=11)){
    x<- 2
  }
  else if ((11<x)&(x<=17)){
    x<- 3
  }
  else{
    x<- 4
  }
  x<- factor(x,levels = c(1,2,3,4) , labels = c("early morning","morning","noon","night"))
}


flat_file$timestamp <-sapply (time, distinct_time)

factor_function <-function(x){
  if(x<3){
    x<- 1
  }
  else if (x>3){
    x<- 3
  }
  else{
    x<- 2
  }
  x<- factor(x,levels = c(1,2,3) , labels = c("low","med","high"))
}

flat_file$topics_match <-sapply (flat_file$topics_match, factor_function)
summary(flat_file$topics_match)

factor_function2 <-function(x){
  if(x<2){
    x<- 1
  }
  else if (x>3){
    x<- 3
  }
  else{
    x<- 2
  }
  x<- factor(x,levels = c(1,2,3) , labels = c("low","med","high"))
}

flat_file$category_not_match <-sapply (flat_file$category_not_match, factor_function2)
summary(flat_file$category_not_match)

factor_function3 <-function(x){
  if(x<0.7){
    x<- 1
  }
  else if (x>1.3){
    x<- 3
  }
  else{
    x<- 2
  }
  x<- factor(x,levels = c(1,2,3) , labels = c("low","med","high"))
}

flat_file$SUM_match_category <-sapply (flat_file$SUM_match_category, factor_function3)
summary(flat_file$SUM_match_category)

flat_file$SUM_NOT_match_category <-sapply (flat_file$SUM_NOT_match_category, factor_function3)
summary(flat_file$SUM_NOT_match_category)

flat_file$category_match <-sapply (flat_file$category_match, factor_function3)
summary(flat_file$category_match)

flat_file$SUM_NOT_match_topics <-sapply (flat_file$SUM_NOT_match_topics, factor_function3)
summary(flat_file$SUM_NOT_match_topics)

summary(flat_file)

factor_function4 <-function(x){
  if(x<12){
    x<- 1
  }
  else if (x>23){
    x<- 3
  }
  else{
    x<- 2
  }
  x<- factor(x,levels = c(1,2,3) , labels = c("low","med","high"))
}

flat_file$topics_not_match <-sapply (flat_file$topics_not_match, factor_function4)
summary(flat_file$topics_not_match)


factor_function5 <-function(x){
  if(x<0.3){
    x<- 1
  }
  else if (x>0.65){
    x<- 3
  }
  else{
    x<- 2
  }
  x<- factor(x,levels = c(1,2,3) , labels = c("low","med","high"))
}

flat_file$SUM_match_topics <-sapply (flat_file$SUM_match_topics, factor_function5)
summary(flat_file$SUM_match_topics)

factor_function6 <-function(x){
  if(x<5){
    x<- 1
  }
  else if (x>9){
    x<- 3
  }
  else{
    x<- 2
  }
  x<- factor(x,levels = c(1,2,3) , labels = c("low","med","high"))
}

flat_file$Num_of_ads <-sapply (flat_file$Num_of_ads, factor_function6)
summary(flat_file$Num_of_ads)

summary(flat_file)



######-------------------- liniar regration algorithm -> Map = 0.5408 ---------------------------

test <- merge(flat_file, event_test, by = 'display_id')
train<-merge(flat_file, event_train, by = 'display_id')

setDF(test)
str(test)
ncol(test)

liniar <-lm (clicked ~  display_id + ad_id + document_id_source + document_id_target 
                +platform + campaign_id + advertiser_id + SUM_match_category
                +SUM_NOT_match_category + topics_match + topics_not_match + SUM_match_topics 
               +SUM_NOT_match_topics + category_match+category_not_match + Num_of_ads,
                data=train)

summary(liniar)


#predicted num between 0-1
str(test)
ncol(test)

predicted  <-predict(liniar, test[,-5], type ="response", se.fit=FALSE)

predicted01 <-ifelse (predicted>0.5,1,0)

actual01 <-test$clicked


# MAP FUNCTUION CACULATION

setDT(test)
test$pred <-predicted
ncol(test)
str(test)
test<-test[ , c(3,2,1,4,6:ncol(test),5)]
str(test)
test<- test[ order(display_id, -pred), ]

test[,valRank:=rank(-pred),by="display_id"]
test$division <-test$clicked/test$valRank


test_di_counter <-distinct(test, display_id)
sum_division <-sum(test$division)
map<-sum_division/ count(test_di_counter)
map


######-------------------- logmodel algorithm -> Map =  0.5431---------------------------

str(flat_file)
spliter <-distinct(flat_file,display_id, keep_all = FALSE)

####split the display ID for test and train
vec<-runif(nrow(spliter)) 
split<-vec >0.3 
split

event_train<-spliter[split,1] #the comma means we split by rows and not with columns.
event_test<-spliter[!split,1] #! means convert the logical value. true become false and the opposit.

test <- merge(flat_file, event_test, by = 'display_id')
train<-merge(flat_file, event_train, by = 'display_id')

setDF(test)
str(test)
ncol(test)

logmodel <-glm (clicked ~  display_id + ad_id + document_id_source + document_id_target 
                +platform + campaign_id + advertiser_id + SUM_match_category
                +SUM_NOT_match_category + topics_match + topics_not_match + SUM_match_topics 
                +SUM_NOT_match_topics + category_match+category_not_match + Num_of_ads,
                data=train)

summary(logmodel)


#predicted num between 0-1
str(test)
ncol(test)

predicted  <-predict(logmodel, test[,-5], type ="response", se.fit=FALSE)
predicted[0:10]
predicted01 <-ifelse (predicted>0.5,1,0)

actual01 <-test$clicked

#install.packages("partitions")
library(partitions)

#install.packages("data.table",repos= c("http://R-Forge.R-project.org", getOption("repos")))

library(data.table)

setDT(test)
test$pred <-predicted
ncol(test)
str(test)
test<-test[ , c(3,2,1,4,6:ncol(test),5)]
str(test)
test<- test[ order(display_id, -pred), ]

test[,valRank:=rank(-pred),by="display_id"]
test$division <-test$clicked/test$valRank
test[1:50]

test_di_counter <-distinct(test, display_id)
sum_division <-sum(test$division)
map<-sum_division/ count(test_di_counter)
map


######-------------------- Random Forest -> Map = 0.5868 ---------------------------

test <- merge(flat_file, event_test, by = 'display_id')
train<-merge(flat_file, event_train, by = 'display_id')

setDF(test)
str(test)
ncol(test)

install.packages("ipred")
library(randomForest)
library(ipred)
randomForest <-randomForest(clicked ~  display_id + ad_id + document_id_source + document_id_target 
                        +platform + campaign_id + advertiser_id + SUM_match_category
                        +SUM_NOT_match_category + topics_match + topics_not_match + SUM_match_topics 
                        +SUM_NOT_match_topics +category_match+category_not_match + Num_of_ads, data=train, mtry=3,
                        importance=TRUE, na.action=na.omit)


summary(randomForest)


#predicted num between 0-1
str(test)
ncol(test)

predicted  <-predict(randomForest, test[,-5], type ="response", se.fit=FALSE)

predicted01 <-ifelse (predicted>0.5,1,0)

actual01 <-test$clicked


# MAP FUNCTUION CACULATION

setDT(test)
test$pred <-predicted
ncol(test)
str(test)
test<-test[ , c(3,2,1,4,6:ncol(test),5)]
str(test)
test<- test[ order(display_id, -pred), ]

test[,valRank:=rank(-pred),by="display_id"]
test$division <-test$clicked/test$valRank
test[1:50]

test_di_counter <-distinct(test, display_id)
sum_division <-sum(test$division)
map<-sum_division/ count(test_di_counter)
map





